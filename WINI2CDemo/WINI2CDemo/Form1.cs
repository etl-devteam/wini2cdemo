﻿using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;


namespace WINI2CDemo
{
    public partial class Form1 : Form
    {
        private class VisionARDLL
        {
            [DllImport("deviceFB.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
            public static extern bool Startup();
            [DllImport("deviceCDC.dll", CharSet = CharSet.Unicode, ExactSpelling = true)]
            public static extern bool StartupCdc();
            [DllImport("deviceFB.dll", EntryPoint = "CloseFb", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool CloseDevFb();
            [DllImport("deviceCDC.dll", EntryPoint = "CloseCdc", ExactSpelling = false, CallingConvention = CallingConvention.Cdecl)]
            public static extern bool CloseDevCdc();
        }


        public Form1()
        {
            InitializeComponent();
            if (!VisionARDLL.Startup() || !VisionARDLL.StartupCdc())
            {
                MessageBox.Show("Please make sure the glasses are connected", "Glasses not found", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Environment.Exit(-1);
            }
        }
    }
}
